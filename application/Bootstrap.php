<?php
/**
 * Bootstrao Class
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Zend Loader
     *
     * @return void
     */
    protected function _initAutoLoader()
    {
        Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);
    }

    /**
     * Multdb
     *
     * @return void
     */
    protected function _initDatabase()
    {
        /* @var $resource Zend_Application_Resource_Multidb */
        $resource = $this->getPluginResource('multidb');
        $resource->init();
        Zend_Registry::set('db1', $resource->getDb('db1'));
    }

    protected function _initRestRoute()
    {
        $this->bootstrap('Request');
        $front = $this->getResource('FrontController');
        $restRoute = new Zend_Rest_Route($front, array(), array(
            'default' => array('version')
        ));
        $front->getRouter()->addRoute('rest', $restRoute);
    }

    protected function _initRequest()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $request = $front->getRequest();
        if (null === $front->getRequest()) {
            $request = new Zend_Controller_Request_Http();
            $front->setRequest($request);
        }
        return $request;
    }

}

