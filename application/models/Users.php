<?php

/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 24/08/2017
 * Time: 16:01
 */
class Users extends Zend_Db_Table_Abstract
{

    protected $_name = "tb_users";
    protected $_primary = "id";

    /**
     * Method the seach
     *
     * @param array|null $params Receive array to do search
     * @param bool|false $all If true bring all register of the database
     * @param bool|false $join If true do joins in other tables
     *
     * @return array|null|Zend_Db_Table_Row_Abstract|Zend_Db_Table_Rowset_Abstract
     */
    public function search(array $params = null, $all = false, $join = false)
    {
        $select = $this->select()->from(array('tb_users' => 'tb_users'));

        if (isset($params) && !empty($params)) {
            foreach ($params as $key => $value) {
                $select->where('tb_users.' . $key . " = ? ", $value);
            }
        }

        if ($join) {
            $select->setIntegrityCheck(false);
        }

        if ($all) {
            $query = $this->fetchAll($select);
        } else {
            $query = $this->fetchRow($select);
        }

        if ($query) {
            $query = $query->toArray();
        } else {
            $query = null;
        }

        return $query;
    }

}