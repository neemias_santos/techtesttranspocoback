<?php

/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 23/08/2017
 * Time: 18:33
 */
class Api_UsersController extends Business_RestRestricted
{

    /**
     * The head action handles HEAD requests and receives an 'id' parameter; it
     * should respond with the server resource state of the resource identified
     * by the 'id' value.
     */
    public function headAction()
    {
        // TODO: Implement headAction() method.
    }

    public function indexAction()
    {
        if ($this->getRequest()->getParams()) {
            $usersBusiness = new Business\UsersBusiness();
            $data = $usersBusiness->simpleClean($this->getRequest()->getPost());


            $result = $usersBusiness->read($data, true);

            if ($result->getType() == 1) {
                $this->getResponse()->appendBody(json_encode(array('users' => $result->getData())));
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $this->getResponse()->appendBody(json_encode($result->getData()));
                $this->getResponse()->setHttpResponseCode(400);
            }
        }
    }

    public function getAction()
    {
        $this->getResponse()->setBody('Get!');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function postAction()
    {
        if ($this->getRequest()->getParams()) {
            $usersBusiness = new Business\UsersBusiness();
            $data = $usersBusiness->simpleClean($this->getRequest()->getParams());

            $result = $usersBusiness->create($data);

            if ($result->getType() == 1) {
                $return = array(
                    'type' => $result->getType(),
                    'data' => $result->getData()
                );
                $this->getResponse()->appendBody(json_encode($return));
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $return = array(
                    'type' => $result->getType(),
                    'data' => $result->getData()
                );
                $this->getResponse()->appendBody(json_encode($return));
                $this->getResponse()->setHttpResponseCode(400);
            }
        }
    }

    public function putAction()
    {
        if ($this->getRequest()->getParams()) {
            $usersBusiness = new Business\UsersBusiness();
            $data = $usersBusiness->simpleClean($this->getRequest()->getParams());

            $result = $usersBusiness->edit($data);

            if ($result->getType() == 1) {
                $return = array(
                    'type' => $result->getType(),
                    'data' => $result->getData()
                );
                $this->getResponse()->appendBody(json_encode($return));
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $return = array(
                    'type' => $result->getType(),
                    'data' => $result->getData()
                );
                $this->getResponse()->appendBody(json_encode($return));
                $this->getResponse()->setHttpResponseCode(400);
            }
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParams()) {
            $usersBusiness = new Business\UsersBusiness();
            $data = $usersBusiness->simpleClean($this->getRequest()->getParams());

            $result = $usersBusiness->delete($data);

            if ($result->getType() == 1) {
                $return = array(
                    'type' => $result->getType(),
                    'data' => $result->getData()
                );
                $this->getResponse()->appendBody(json_encode($return));
                $this->getResponse()->setHttpResponseCode(200);
            } else {
                $return = array(
                    'type' => $result->getType(),
                    'data' => $result->getData()
                );
                $this->getResponse()->appendBody(json_encode($return));
                $this->getResponse()->setHttpResponseCode(400);
            }
        }
    }


}