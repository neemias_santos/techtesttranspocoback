<?php
header("Access-Control-Allow-Origin", "*");
header("Access-Control-Allow-Headers", "Origin, X-Request-Width, Content-Type, Accept");
header("Access-Control-Allow-Headers", "Access-Control-Allow-Methods: GET, POST, OPTIONS");
class IndexController extends Zend_Controller_Action
{

    public function headAction()
    {
        $this->getResponse()->setBody('Hello World');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function indexAction()
    {
        if ($this->getRequest()->getParams()) {
            $data = $this->getRequest()->getParams();
            return $this->_helper->json->sendJson($data);
        }
        $this->getResponse()->setBody('Hello World');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function getAction()
    {
        $this->getResponse()->setBody('Get!');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function postAction()
    {
        $this->getResponse()->setBody('resource created');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function putAction()
    {
        $this->getResponse()->setBody('resource updated');
        $this->getResponse()->setHttpResponseCode(200);
    }

    public function deleteAction()
    {
        $this->getResponse()->setBody('resource deleted');
        $this->getResponse()->setHttpResponseCode(200);
    }

}