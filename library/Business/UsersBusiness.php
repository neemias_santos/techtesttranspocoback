<?php
/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 23/08/2017
 * Time: 18:50
 */

namespace Business;

use Business\Business;
use Business\ResultBusiness;

class UsersBusiness extends Business
{
    /**
     * Default research
     *
     * @param array|null $data
     * @param bool $all
     * @param bool $join
     * @return ResultBusiness
     */

    public function read(array $data = null, $all = false, $join = false)
    {
        $model = new \Users();

        $dados = $model->search($data, $all, $join);

        if ($dados || empty($dados)) {
            return new ResultBusiness(1, $dados);
        } else {
            return new ResultBusiness(2, 'Without data');
        }
    }

    /**
     * Register user
     *
     * @param array $data
     * @return bool|ResultBusiness
     * @throws \Zend_Db_Table_Exception
     * @throws \Zend_Exception
     */
    public function create(array $data)
    {
        try {
            $usersModel = new \Users();

            if (isset($data['mail']) && !empty($data['mail'])) {
                //check if the user already registered
                $checkMail = $usersModel->search(array('mail' => $data['mail']));
            }

            if (isset($checkMail)) {
                return new ResultBusiness(2, 'User already registered');
            }

            //Save the users
            $usersModel->insert($data);

            return new ResultBusiness(1, 'User register with success');

        } catch (\Zend_Exception $e) {
            return new ResultBusiness(2, 'User not registered with success: '. $e->getMessage());
        }
    }

    public function edit(array $data)
    {
        try {
            $usersModel = new \Users();

            //add date update
            $date = new \Zend_Date();
            $data['updated_at'] = $date->get('YYYY-MM-dd HH:mm:ss');

            //Save data
            $usersModel->update($data, 'id=' . $data['id']);

            return new ResultBusiness(1, 'User updated with success');

        } catch (\Zend_Exception $e) {
            return new ResultBusiness(2, 'Error while updating: '. $e->getMessage() );
        }
    }

    public function delete(array $data)
    {
        try {
            $usersModel = new \Users();

            //Delete users
            $usersModel->delete('id=' . $data['id']);

            return new ResultBusiness(1, 'User delete with success');

        } catch (\Zend_Exception $e) {
            return new ResultBusiness(2, 'Error while deleting: '. $e->getMessage());
        }
    }

}