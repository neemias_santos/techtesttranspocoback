<?php

/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 23/08/2017
 * Time: 18:40
 */

namespace Business;

abstract class Business
{
    function __construct()
    {
        $serverUrlHelper = new \Zend_View_Helper_ServerUrl();
        $this->_baseUrl = $serverUrlHelper->serverUrl();
    }


    /**
     * Remove unnecessary data
     *
     * @param $data
     * @return array
     */
    public function simpleClean($data)
    {
        if (isset($data['controller'])) {
            unset($data['controller']);
        }
        if (isset($data['module'])) {
            unset($data['module']);
        }
        if (isset($data['action'])) {
            unset($data['action']);
        }

        return $data;
    }
}